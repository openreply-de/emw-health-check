/**
 * Created by dominikriedel on 10/12/18.
 */

const _ = require('lodash');

/**
 * wrap response to intercept on res.end, this will call a callback method provided by the caller to return the original res object.
 *
 * @param {Object} req the express request object (see http://expressjs.com/en/4x/api.html#req)
 * @param {Object} res the express response object (see http://expressjs.com/en/4x/api.html#res)
 * @params {Function} callback returning the original response object
 */
module.exports.wrapResponse = async (req, res, callback) => {
  const original = {};

  if (_.has(res, 'end')) { original.end = res.end; }

  function deactivateWrapper() {
    delete res.end;
    Object.assign(res, original);
  }
  res.end = function end(chunk = undefined, encoding = undefined, endCallback = undefined) {
    deactivateWrapper();

    if (callback) callback(res);
    return this.end(chunk, encoding, endCallback);
  };
};
