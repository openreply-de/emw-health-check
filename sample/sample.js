/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved */
const express = require('express');
/* eslint-enable import/no-extraneous-dependencies, import/no-unresolved */
const healthCheck = require('..');

const app = express();

// initializes our healthCheck middleware. will also enable a GET /health endpoint.
app.use(healthCheck({ serviceName: 'testService', serviceVersion: '1.0.0' }));

app.get('/healthy', (req, res) => {
  res.status(200).json({
    status: 'success'
  });
});

app.get('/unhealthy', (req, res) => {
  res.status(500).json({
    status: 'error'
  });
});

app.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('Example app listening on port 3000!');
});
