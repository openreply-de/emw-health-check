
## _
Created by dominikriedel on 10/12/18.

**Kind**: global constant  
## healthCheck(options) ⇒ RequestMiddleware
Enables GET /health endpoint to retrieve the health of the service.

This middleware also intercepts the response of every request made to the service to check if there is an ongoing problem
with a steady response status of HTTP 500 (internal server error). If so the service health will switch to `unhealthy`.

By default all API endpints (except the GET /health) endpoint will be monitored.
By default the service will switch to and `unhealthy` state if the service will responde with HTTP status 500 for 10 sequential requests.

**Kind**: global function  
**Throws**:

- TypeError in case that the middleware has been passed incorrectly to express
- TypeError if not passed an options object
- TypeError if not given a serviceName with the options


| Param | Type | Description |
| --- | --- | --- |
| options | RequestIdConfiguration | configuration options for this middleware |

## wrapResponse(req, res)
wrap response to intercept on res.end, this will call a callback method provided by the caller to return the original res object.

**Kind**: global function  
**Params**: callback  

| Param |
| --- |
| req | 
| res | 

## HealthCheckConfiguration : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| serviceName | string | name of the service that will be appended to the trace id string |
| checkInternalErrors | string | whether or not to check for internal server errors on all endpoints. (default: true) |
| errorThreshold | string | the amount of internal server errors that may happen before health of service is considered unhealthy (default: 10) |

## RequestMiddleware : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| req | Object | the express request object (see http://expressjs.com/en/4x/api.html#req) |
| res | Object | the express response object (see http://expressjs.com/en/4x/api.html#res) |
| next | Object | the next middleware object to be executed from the express middleware list |

