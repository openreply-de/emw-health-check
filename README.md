# Health Check Express Middleware

Health checks are one of the most essential features when it comes to distributed systems. This package provides an express middleware that enables an health checker (e.g. load balancer) to sequentially monitor the health of your service via REST API. Therefor this middleware enables an API endpoint to provide steady information about the health of your service. By default the health endpoint will be provided under `GET /health`.     

If the express server is running and reachable the middleware will provide and success answer (HTTP Status: 200) with information regarding the service (e.g. service name, environment variables, ...). This middleware also listens on all outgoing responses and provides works as a circuit breaker for the service if any endpoint is steadily responding with HTTP Status 500.
 
By default the healthCheck middleware will listen on every HTTP endpoint registred to the express app. 
  
## Getting Started

### Installing

you can install this package via npm

`npm i @openreply/emw-health-check`

### Usage

Simply add this module to your middleware chain

```javascript
app.use(healthCheck({ serviceName: 'testService' }));
```

See the following usage example

```javascript
/* eslint-disable import/no-extraneous-dependencies */
const request = require('request');
const express = require('express');
/* eslint-enable import/no-extraneous-dependencies */
const healthCheck = require('@openreply/emw-health-check');

const app = express();

// initializes our healthCheck middleware. will also set the trace header on
// the response object by default
app.use(healthCheck({ serviceName: 'testService' }));

app.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('Example app listening on port 3000!');
});

```

## Running the tests

You can run the test suite via

`npm test`

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/openreply-de/emw-health-check). 

## Authors

* **Dominik Riedel** - *Initial work* - [openreply-de](https://bitbucket.org/openreply-de/emw-health-check)

See also the list of [authors](AUTHORS.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## References

- Information on [CircuitBreaker](https://martinfowler.com/bliki/CircuitBreaker.html) Pattern 
