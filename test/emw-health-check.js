const { expect } = require('chai');
const sinon = require('sinon');
const healthCheck = require('..');

describe('emw-health-check', () => {
  it('should throw if passed purely as a reference into the middleware chain', () => {
    expect(() => healthCheck({}, {}, () => {})).to.throw(TypeError);
  });

  describe('usage', () => {
    it('should require an options object otherwise throw', () => {
      expect(() => healthCheck()).to.throw(/expects to be passed an options object/);
    });

    it('should require an service name and otherwise throw', () => {
      expect(() => healthCheck({})).to.throw(/you need to pass a serviceName/);
    });

    it('should require an service name and otherwise throw', () => {
      expect(() => healthCheck({ serviceName: 'test' })).to.throw(/you need to pass a serviceVersion/);
    });

    it('should export a function', () => {
      expect(healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })).to.be.a('function');
    });
  });

  describe('no health check', () => {
    it('should call the next middleware after executing', () => {
      const next = sinon.spy();
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({ get: () => {}, path: '/test' }, { end: () => {} }, next);
      expect(next.calledOnce).to.be.true;
    });
  });

  describe('healthy', () => {
    it('should not to call the next middleware after executing GET /health', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get'
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(200);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('healthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?name=test', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'test' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(200);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('healthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?version=1.0.0', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { version: '1.0.0' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(200);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('healthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?name=test&version=1.0.0', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'test', version: '1.0.0' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(200);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('healthy');
      expect(next.calledOnce).not.to.be.true;
    });
  });

  describe('unhealthy', () => {
    it('should not to call the next middleware after executing GET /health?name=wrong', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'wrong' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(500);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('unhealthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?version=wrong', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { version: 'wrong' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(500);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('unhealthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?name=test&version=wrong', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'test', version: 'wrong' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(500);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('unhealthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?name=wrong&version=1.0.0', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'test', version: 'wrong' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(500);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('unhealthy');
      expect(next.calledOnce).not.to.be.true;
    });

    it('should not to call the next middleware after executing GET /health?name=wrong&version=wrong', () => {
      const next = sinon.spy();
      let response;
      healthCheck({ serviceName: 'test', serviceVersion: '1.0.0' })({
        get: () => {}, path: '/health', method: 'get', query: { name: 'wrong', version: 'wrong' }
      }, { end: () => {}, status: code => ({ json: (body) => { response = { code, body }; } }) }, next);
      expect(response.code).to.equal(500);
      expect(response.body).to.be.an('object');
      expect(response.body.service).to.equal('test');
      expect(response.body.status).to.equal('unhealthy');
      expect(next.calledOnce).not.to.be.true;
    });
  });
});
